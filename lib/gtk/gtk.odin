package gtk

import "core:c"
//////////////////////////////
// GObject
//////////////////////////////

guint :: #type c.uint
GType :: #type c.ulong
GData :: struct {}
GClosure :: struct{}
GTypeInstance :: struct {}
GApplication :: struct {}

GCallback :: #type proc "c" ()
GClosureNotify :: #type proc "c" (data: rawptr, closure: ^GClosure)

GConnectFlags :: bit_set[GConnectFlag]
GConnectFlag :: enum {
  AFTER = 1,
  SWAPPED = 2,
}

GApplcationFlags :: bit_set[GApplicationFlag]
GApplicationFlag :: enum {
  NONE,
  IS_SERVICE  = NONE,
  IS_LAUNCHER,

  HANDLES_OPEN,
  HANDLES_COMMAND_LINE,
  SEND_ENVIRONMENT,

  NON_UNIQUE,

  CAN_OVERRIDE_APP_ID,
  ALLOW_REPLACEMENT,
  REPLACE,
}

G_TYPE_CHECK_INSTANCE_CAST :: #force_inline proc "c" (obj: rawptr, g_type: GType, $T: typeid) -> ^T {
  return cast(^T) g_type_check_instance_cast(cast(^GTypeInstance)obj, g_type)
}

g_signal_connect :: #force_inline proc "c" (instance: rawptr, detailed_signal: ^c.char, c_handler: GCallback, data: rawptr) -> u64 {
  return g_signal_connect_data(instance, detailed_signal, c_handler, data, nil, {})
}

@(default_calling_convention = "c")
foreign {
  g_signal_connect_data :: proc (instance: rawptr, detailed_signal: ^c.char, c_handler: GCallback,
    data: rawptr, destroy_data: GClosureNotify, connect_flags: GConnectFlags) -> c.ulong ---

  g_application_get_type :: proc () -> GType ---

  g_application_run :: proc (application: ^GApplication, argc: c.int, argv: ^^c.char) -> c.int ---
  
  g_object_unref :: proc (object: rawptr) ---

  g_type_check_instance_cast :: proc (instance: ^GTypeInstance, iface_type: GType) -> ^GTypeInstance ---
}

//////////////////////////////
// GTK
//////////////////////////////

Application :: struct {}

Widget :: struct {}
Window :: struct {}

WINDOW :: #force_inline proc "c" (obj: ^Widget) -> ^Window {
  window_type := window_get_type()
  return G_TYPE_CHECK_INSTANCE_CAST(cast(rawptr)obj, window_type, Window)
}

APPLICATION :: #force_inline proc "c" (obj: ^Application) -> ^GApplication {
  application_type := g_application_get_type()
  return G_TYPE_CHECK_INSTANCE_CAST(cast(rawptr)obj, application_type, GApplication)
}

@(default_calling_convention = "c")
foreign {
  @(link_name="gtk_application_new")
  application_new :: proc (application_id: ^c.char, flags: GApplcationFlags) -> ^Application ---

  @(link_name="gtk_application_window_new")
  application_window_new :: proc (application: ^Application) -> ^Widget ---

  @(link_name="gtk_window_set_title")
  window_set_title :: proc (window: ^Window, title: ^c.char) ---

  @(link_name="gtk_window_set_default_size")
  window_set_default_size :: proc (window: ^Window, width, heigh: c.int) ---

  @(link_name="gtk_window_get_type")
  window_get_type :: proc () -> GType ---

  @(link_name="gtk_widget_show")
  widget_show :: proc (widget: ^Widget) ---
}
