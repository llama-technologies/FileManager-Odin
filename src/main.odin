/**
 * @author  Pablo Narvaja
 * @date    24/03/22
 * @license gpl3
*/
package main

import "common"
import "lib:gtk"
import "core:c"
import "core:os"
import "core:log"
import str "core:strings"

WINDOW_TITLE :: "File Manager"
APP_NAME :: "File Manager"
APP_ID :: "org.llama.filemanager"

main :: proc() {
  status : c.int = 0
  defer os.exit(cast(int)status)

  context = common.init_logger(APP_NAME + ".log", APP_NAME)
  defer common.deinit_logger()

  app_id := str.clone_to_cstring(APP_ID)
  defer delete(app_id)

  application := gtk.application_new(transmute(^c.char)app_id, {.NONE})
  if application == nil do log.panic("Could not create GtkApplication")
  defer gtk.g_object_unref(application)

  activate_signal := cstring("activate")
  gtk.g_signal_connect(application, transmute(^c.char)activate_signal, cast(gtk.GCallback) activate, nil)
  log.info("Signal \"activate\" connected")

  status = gtk.g_application_run(gtk.APPLICATION(application), 0, nil)
}

activate :: proc "c" (app: ^gtk.Application, user_data: rawptr) {
  context = common.get_logger()

  window_title := str.clone_to_cstring(WINDOW_TITLE)
  defer delete(window_title)

  window := gtk.application_window_new(app)
  if window == nil do log.panic("Could not create GtkWindow")
  log.info("Window created!")

  gtk.window_set_title(gtk.WINDOW(window), transmute(^c.char)window_title)
  gtk.window_set_default_size(gtk.WINDOW(window), 200, 200)
  gtk.widget_show(window)
}
